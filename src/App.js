import React, { Component } from 'react';
import logo from "./images/maxiv_logo.png";
import { Scanlog } from "./Scanlog";
import { Login } from "./Login";
import { Button } from 'react-bootstrap';
import './global.js';
import Cookies from 'universal-cookie';
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";
import { Alert } from 'react-bootstrap'

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            beamline: "Please select a beamline",
            homepage: true,
            options: null,
            loginShown: false,
            loggedIn: false,
            token: "",
            username: "",
            showPopUp: false,
            popUpVariant: "",
            popUpMessage: "",
        };

        this.select = this.select.bind(this);
        this.toggleLoginModal = this.toggleLoginModal.bind(this);
        this.loginSSO = this.loginSSO.bind(this);
        this.loggingIn = this.loggingIn.bind(this);
        this.logout = this.logout.bind(this);
        this.popUp = this.popUp.bind(this);
    }

    componentDidMount() {
        //Get login from cookies and set in state
        const queryParams = new URLSearchParams(window.location.search);
        if (queryParams.get("access-token")){
            this.loggingIn(queryParams.get("access-token"))
        } else {
            const cookies = new Cookies();
            typeof (cookies.get("username")) != "undefined" && this.setState({ username: cookies.get("username") });
            typeof (cookies.get("token")) != "undefined" && this.setState({ token: cookies.get("token"), loggedIn: true }) || this.loggingIn(cookies.get("token"));
    
        }


        const pathname = window.location.pathname.replace("/", "")

        for (let k = 0; k < global.beamline.length; k++) {
            if (global.beamline[k].toLowerCase() == pathname) {
                this.setState(
                    {
                        beamline: global.beamline[k],
                        homepage: false
                    }
                )
            }
        }

    }

    select(e) {
        this.setState({ beamline: e.target.value, homepage: false });
    }

    toggleLoginModal() {
        this.setState({ loginShown: !this.state.loginShown });
    }

    loginSSO () {
        document.location.href = global.scicat_url + "/api/v3/auth/oidc";
    }

    loggingIn(token) {
        let url = global.scicat_url + "/api/v3/auth/whoami"
        const requestOptions = {
            headers: { 'Authorization': 'Bearer ' + token },
        };
        fetch(url, requestOptions)
        .then(response => response.json())
        .then((data) => {
            if (data.username){
                this.setState({ loggedIn: true, token: token, username: data.username });
                const options = []
                for (let k = 0; k < global.beamline.length; k++) {
                    const link = "/" + global.beamline[k].toLowerCase();
                    options[k] = [<Link to={link} key={k}>
                        <Button className="beamlineselect" value={global.beamline[k]} onClick={this.select}>{global.beamline[k]}</Button>
                    </Link>]
                }
                this.setState({ options: options });
                const cookies = new Cookies();
                cookies.set("username", data.username, { path: '/', sameSite: "none", secure: true, maxAge: 86400 });
                cookies.set("token", token, { path: '/', sameSite: "none", secure: true, maxAge: 86400 });
            }
        });
    }

    logout() {
        this.setState({ loggedIn: false, token: null, username: null });

        const cookies = new Cookies();
        cookies.remove("username");
        cookies.remove("token");
    }

    reloadPage() {
        window.location = window.location.origin
    }

    popUp(variant, message){
        this.setState({upload: false, showPopUp: true, popUpVariant: variant, popUpMessage: message});
        setTimeout(() => {this.setState({showPopUp: false})}, 2000)
    }

    render() {
        return (
            <div className="App">
                <div className="title">
                    <img className="home" src={logo} alt="MAXIV_logo" onClick={this.reloadPage} />&nbsp;&nbsp; SciCat Scanlog - {this.state.beamline}
                    <div className="home_right">
                        {this.state.loggedIn ?
                            <div><p id="loggedInAs">Logged in as <i>{this.state.username}</i>&nbsp; &nbsp; </p><Button variant="secondary" onClick={this.logout}>Log out</Button></div>
                            :
                            <Button variant="secondary" onClick={this.loginSSO}>Login</Button>
                        }
                    </div>
                </div>
                <div className="adminLogin">
                {this.state.loggedIn ?
                            <p></p>
                            :
                            <a onClick={this.toggleLoginModal}>Admin</a>
                        }
                </div>
                <Login
                    loginShown={this.state.loginShown}
                    loginHide={this.toggleLoginModal}
                    loggedIn={this.loggingIn}
                />
                {this.state.homepage ?
                    <>
                    {this.state.loggedIn ?
                    <div className="content2">
                    <Router>
                        {this.state.options}
                    </Router>
                    </div>
                    :
                    <div className="content2">You need to log in to access Scanlog</div>
                    }
                    </>
                    :
                    <Scanlog
                        token={this.state.token}
                        beamline={this.state.beamline}
                        loggedIn={this.state.loggedIn}
                        username={this.state.username}
                        popUp={this.popUp}
                    />
                }
                {this.state.showPopUp &&
                    <div id="popup">
                        <Alert key="popup" variant={this.state.popUpVariant}>
                            {this.state.popUpMessage}
                        </Alert>
                    </div>
                }

            </div>
        )
    }

}
export default App;
