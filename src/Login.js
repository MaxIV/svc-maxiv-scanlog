import React, { Component } from "react";
import { Modal } from "react-bootstrap";

export class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            login_failed: false,
        }

        this.onSubmit = this.onSubmit.bind(this)
    }

    onSubmit(e) {
        e.preventDefault();

        const form_data = {
            username: e.target.username.value.trim(),
            password: e.target.password.value.trim()
        }

        const login_url = global.scicat_url + "/api/v3/Users/login";

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(form_data)
        };

        fetch(login_url, requestOptions)
            .then(response => response.json())
            .then(data => {
                if (data.access_token || data.id) {
                    this.setState({ login_failed: false });
                    this.props.loginHide();
                    this.props.loggedIn(data.access_token);
                } else {
                    this.setState({ login_failed: true });
                }
            })
            .catch(error => this.setState({ login_failed: true }))
    }

    render() {
        return (
            <Modal
                show={this.props.loginShown}
                onHide={this.props.loginHide}
            >
                <div id="LoginForm">
                    <form onSubmit={this.onSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Sign in</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            {this.state.login_failed &&
                                <div className="alert alert-danger" role="alert">
                                    Wrong username and/or password.
                                </div>
                            }
                            <div className="form-group">
                                <label>Username</label>
                                <input type="text" name="username" className="form-control" placeholder="Enter username" />
                            </div>

                            <div className="form-group">
                                <label>Password</label>
                                <input type="password" name="password" className="form-control" placeholder="Enter password" />
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <button type="submit" className="btn btn-primary btn-block">Submit</button>
                        </Modal.Footer>
                    </form>
                </div>
            </Modal>
        );
    }
}
