global.beamline = ["CoSAXS", "FemtoMAX", "FlexPES", "DanMAX", "Veritas", "MAXPEEM", "NanoMAX", "FinEstBeAMS", "Balder", "HIPPIE", "ForMAX", "SPECIES", "BioMAX", "MicroMAX", "SoftiMAX", "BLOCH"];
global.scicat_url = "https://scicat.maxiv.lu.se"

global.elogy_url = "https://elogy.maxiv.lu.se:443/api";
global.elogy_proxyurl = "https://elogy-proxy-scanlog.apps.okd.maxiv.lu.se/api";

global.fields = [
    { id: 0, label: "ScanID", value: "scanID", width: "90px", widthFixed: true, sortable: true },
    { id: 1, label: "Start time", value: "start_time", width: "160px", widthFixed: false, sortable: true },
    { id: 2, label: "PI", value: "pi", width: "200px", widthFixed: false, sortable: true },
    { id: 3, label: "Proposal", value: "proposal", width: "100px", widthFixed: true, sortable: true },
    { id: 4, label: "Name", value: "name", width: "150px", widthFixed: false, sortable: true },
    { id: 5, label: "Scan", value: "scan", width: "180px", widthFixed: false, sortable: true },
    { id: 6, label: "Detectors", value: "detectors", width: "200px", widthFixed: false, sortable: true },
    { id: 7, label: "Data path", value: "path", width: "150px", widthFixed: false, sortable: true },
    { id: 8, label: "SciCat", value: "scicat", width: "60px", widthFixed: true, sortable: false },
    { id: 9, label: "Comment", value: "comment", width: "250px", widthFixed: false, sortable: false },
    { id: 10, label: "Upload Image", value: "upload_img", width: "200px", widthFixed: false, sortable: false },
    { id: 11, label: "Images", value: "show_img", width: "70px", widthFixed: true, sortable: false },
    { id: 12, label: "Data Quality Metrics", value: "dqm", width: "100px", widthFixed: true, sortable: true }
]

// These are fields that should be skipped in metadata because they are old or are already configured
global.fieldsSkip = ["title", "end time", "scanID", "number of steps", "motor start position", "motor final position", "number of prescansnapshot", "number of prescansnapshots"]
