import React, { Component } from 'react';
import './global.js';
import { Export } from "./Export";
import reset_icon from "./images/arrow_undo.ico";
import Toggle from 'react-toggle';
require("react-toggle/style.css");
import { Container, Button, Row, Col, Form } from "react-bootstrap";
import CreatableSelect from 'react-select/creatable';

export class Elogy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            options: null,
            optionsEntry: null,
            logbook: null,
            entries: null,
            selectedLogbookID: 0,
            selectedLogbook: <i>no logbook selected</i>,
            reset: false,
            newEntry: true,
            selectedEntry: { title: "Choose an entry..." },
            currentPath: "",

            scanGroups: JSON.parse(localStorage.getItem("scanlogGroups")) || [],//why it needs JSON.parse again?
            selectedScanlogGroup: {}
        }

        this.change = this.change.bind(this);
        this.reset = this.reset.bind(this);
        this.newEntryToggle = this.newEntryToggle.bind(this);
        this.changeEntries = this.changeEntries.bind(this);
    }

    componentDidMount() {
        const url = global.elogy_url + "/logbooks/0/";

        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({ data: data.logbook }))
            .catch(error => alert(error))
            .then(() => { this.buildTree() });
    }

    buildTree() {

        let options = [<option value="default" key="default" disabled hidden>Choose a logbook...</option>];
        if (this.state.data == null) {
            return null
        } else {
            for (let k = 0; k < this.state.data.children.length; k++) {
                options[k + 1] = <option value={k} key={this.state.data.children[k].id}>{this.state.data.children[k].name}</option>
            }
            this.setState({ options: options })
            this.setState({ logbook: this.state.data })
        }

    }

    change(e) {
        const new_value = e.target.value;

        if (new_value == "paaseitje") {
            window.open("http://xkcd.com/2341/")
            return null;
        }

        let path = this.state.currentPath + " " + this.state.logbook.children[new_value].name + " --> ";

        this.setState({
            selectedLogbook: this.state.logbook.children[new_value].name,
            logbook: this.state.logbook.children[new_value],
            reset: true,
            currentPath: path
        });

        let logbook = this.state.logbook.children[new_value];
        let options = [<option value="default" key="default" disabled>{this.state.logbook.children[new_value].name}</option>];

        for (let k = 0; k < logbook.children.length; k++) {
            options[k + 1] = <option value={k} key={logbook.children[k].id}>{logbook.children[k].name}</option>
        }

        if (this.state.logbook.children[new_value].name == "KITS") {
            options[options.length + 1] = <option value="paaseitje" key={options.length + 1}>Paaseitje</option>
        }

        this.setState(
            { options: options, selectedLogbookID: this.state.logbook.children[new_value].id },
            function () {
                if (this.state.newEntry === false) {
                    this.retrieveEntries();
                }
            }
        )
    }

    reset() {
        this.buildTree();
        this.setState({
            reset: false,
            currentPath: "",
            selectedLogbook: <i>no logbook selected</i>
        });
    }

    newEntryToggle(e) {
        this.setState({ newEntry: e.target.checked });
        if (e.target.checked === false) {
            this.retrieveEntries();
        }
    }

    retrieveEntries() {
        const url = global.elogy_url + "/logbooks/" + this.state.selectedLogbookID + "/entries/";

        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({ entries: data.entries }))
            .catch(error => alert(error))
            .then(() => { this.buildEntryTree() })
    }

    buildEntryTree() {
        let options = [<option value="default" key="default" selected disabled hidden>Choose an entry...</option>];

        for (let k = 0; k < this.state.entries.length; k++) {
            options[k + 1] = <option value={k} key={this.state.entries[k].id}>{this.state.entries[k].title}</option>;
        }

        this.setState({ optionsEntry: options });
    }

    changeEntries(e) {
        this.setState({ selectedEntry: this.state.entries[e.target.value] })
    }

    handleChangeCreatable = (newValue, actionMeta) => {
        const action = actionMeta.action;

        if (action == "create-option") {
            const scanlogGroups = this.state.scanGroups;
            const index = Math.floor(Math.random() * 12)// there are 13 colors in array for now
            const colors = [
                "red", "blue", "green", "lightgreen", "lightblue", "gray", "lightgrey", "darkgray", "orange", "aqua", "aquamarine", "violet", "turquoise"
            ]
            newValue.color = colors[index];
            scanlogGroups.push(newValue);
            this.setState({ scanGroups: scanlogGroups, selectedScanlogGroup: newValue });
        }

        if (action == "select-option") {
            this.setState({ selectedScanlogGroup: newValue });
        }
    };

    render() {
        return (
            <div id="elogy">
                <Container fluid>
                    <Row>
                        <Col sm={3} md={4}>
                            <div className="fieldset">
                                <span className="legend">
                                    Export
                                </span>
                                {this.props.noDataSelected ?
                                    <span className="legend legendRight"><i>no data selected</i></span>
                                    :
                                    <span className="legend legendRight"> {this.props.selectedRows.length} scan(s)</span>
                                }
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm="3">
                                        <div className="elogy_left">Title: </div>
                                    </Form.Label>
                                    <Col sm="9">
                                        {this.state.newEntry && <input className="form-control" ref="elogy_title" placeholder="Exported from Scanlog" />}
                                        {!this.state.newEntry && <div className="form-control elogy_right">{this.state.selectedEntry.title}</div>}
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm="3">
                                        <div className="elogy_left">Logbook: </div>
                                    </Form.Label>
                                    <Col sm="9">
                                        {this.state.reset && <input type="image" src={reset_icon} alt="reset" onClick={this.reset} />}
                                        {this.state.currentPath}
                                        <select className="form-control" onChange={this.change} value="default">
                                            {this.state.options}
                                        </select>
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row} className="mb-3">
                                    <Col sm={12}>
                                        <Row>
                                            <Col xs={3}>
                                                <Toggle
                                                    checked={this.state.newEntry}
                                                    name='newEntry'
                                                    value='yes'
                                                    onChange={this.newEntryToggle} />
                                            </Col>
                                            <Col xs={9}>
                                                {this.state.newEntry &&
                                                    <div className="elogy_right">Create a new entry in <b>{this.state.selectedLogbook}</b></div>
                                                }
                                                {!this.state.newEntry &&
                                                    <div className="elogy_right">Append to entry:&nbsp;
                                                        <select className="form-control" onChange={this.changeEntries}>
                                                            {this.state.optionsEntry}
                                                        </select>
                                                    </div>
                                                }
                                            </Col>
                                        </Row>
                                    </Col>
                                </Form.Group>
                                <Export
                                    selectedLogbookID={this.state.selectedLogbookID}
                                    selectedLogbook={this.state.selectedLogbook}
                                    selectedRows={this.props.selectedRows}
                                    noDataSelected={this.props.noDataSelected}
                                    logbookSelected={this.state.reset}
                                    activeColumns={this.props.activeColumns}
                                    newEntry={this.state.newEntry}
                                    selectedEntry={this.state.selectedEntry}
                                    refs={this.refs}
                                    username={this.props.username}
                                    popUp={this.props.popUp}
                                />
                            </div>
                        </Col>

                        <Col sm={3} md={4}>
                            <div className="fieldset">
                                <span className="legend">
                                    Create group
                                </span>
                                {this.props.noDataSelected ?
                                    <span className="legend legendRight"><i>no data selected</i></span>
                                    :
                                    <span className="legend legendRight"> {this.props.selectedRows.length} scan(s)</span>
                                }

                                <Form.Group as={Row} className="mb-3">
                                    <Col sm={12}>
                                        <CreatableSelect
                                            isClearable
                                            onChange={this.handleChangeCreatable}
                                            options={this.state.scanGroups}
                                        />
                                        <Row>
                                            <Col xs={12} sm={{ span: 6, offset: 3 }}>
                                                <Button size="sm" variant="success" onClick={() => this.props.onGroupRows(this.state.selectedScanlogGroup, this.props.selectedRows)} disabled={this.props.selectedRows.length === 0 || !this.state.selectedScanlogGroup.value} className='export-btn'>Group {this.props.selectedRows.length !== 0 && `selected ${this.props.selectedRows.length} scan(s)`}</Button>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={12}>
                                                <span>* can't find the group? Type in and create new!</span>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Form.Group>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
