import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import './global.js'

export default class ShowImages extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            show: false,
            showNodata: false
        }
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({ show: true, data: this.props.attachments });
    }

    closeModal() {
        this.setState({ show: false })
    }

    render() {
        return (
            <div>
                {this.props.attachments.length !== 0 && this.props.attachments.map((attachment, index) => index < 4 && (
                    <img key={index} title={attachment.caption} className="showImages" src={attachment.thumbnail} />
                ))}

                <button className="button" onClick={this.openModal}>Show all</button>
                <Modal show={this.state.show} onHide={this.closeModal} size="lg">
                    <Modal.Header closeButton>
                        Images for {this.props.name}
                    </Modal.Header>
                    <Modal.Body>
                        {this.state.data.map((data_item, index) => (
                            <div key={index}>
                                <img title={data_item.caption} className="showImages" src={data_item.thumbnail} />
                                <hr />
                            </div>
                        ))}
                    </Modal.Body>
                </Modal>
            </div>
        );
    }

}
