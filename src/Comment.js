import React, { Component } from 'react';
import './global.js'

export default class Comment extends Component {
    constructor(props) {
        super(props);
        this.refComment = React.createRef();
        this.sendComment = this.sendComment.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    sendComment() {
        const url = global.scicat_url + "/api/v3/Datasets/" + this.props.PID.replace("/", "%2F");
        const comment = this.refComment.current.innerText
        const requestOptions = {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.props.token },
            body: JSON.stringify({"comment": comment})
        };
        fetch(url, requestOptions)
            .then((response) => {
                if (response.status === 200) {
                    this.props.popUp("success", "Your comment has been saved.");
                    this.props.changeComment(this.props.id, comment, comment)
                } else {
                    this.props.popUp("warning", "You don't have the correct access right to write comments");
                }
            })
            .catch(error => alert(error))
    }

    onChange() {
        if (this.refComment.current !== null && this.props.comments.origComment !== this.refComment.current.innerText && this.props.comments.comment !== this.refComment.current.innerText){
            this.props.changeComment(this.props.id, this.props.comment, this.refComment.current.innerText)
        }
    }

    render() {
        return (
            <div>
                <span onMouseLeave={this.onChange} className="textarea" role="textbox" contentEditable="true" suppressContentEditableWarning={true} ref={this.refComment}>
                    {this.props.comments.comment}
                </span>
                {this.props.comments.origComment !== this.props.comments.comment && <button className="button" onClick={this.sendComment}>Save</button>}
            </div>

        );
    }
}
