import React, { Component } from 'react';
import './global.js'

export default class UploadImages extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageCache: {},
            showPopUp: false,
            popUpVariant: "",
            popUpMessage: "",
            upload: false,
        }
        this.uploadImage = this.uploadImage.bind(this);
        this.updateImageSelection = this.updateImageSelection.bind(this);
    }

    updateImageSelection(e) {
        const docID = this.props.PID;
        this.state.imageCache[docID] = e.target.files;//TODOS: why?
        this.setState(({ upload: true }));
    }

    uploadImage() {
        const docID = this.props.PID
        const images = this.state.imageCache[docID]

        try {
            Object.keys(images).map((key, index) => (
                this.readImage(
                    images[key], docID)
            ));
        } catch (err) {
            if (typeof images === 'undefined') {
                alert("No images selected to upload...");
            } else {
                alert("Can't upload..." + err);
            }
        }
    }

    readImage(img, docID) {
        const reader = new FileReader();
        const imgMeta = {
            key: docID,
            type: img.type,
            name: img.name
        }
        reader.onload = this.convertImgToPacketAndSend.bind(this, imgMeta)
        reader.readAsBinaryString(img)
    }

    convertImgToPacketAndSend(imgMeta, readerEvent) {
        const binaryString = readerEvent.target.result;
        let asciiString = btoa(binaryString);
        asciiString = "data:" + imgMeta.type + ";base64," + asciiString;
        this.sendImage(asciiString, imgMeta);
    }

    sendImage(img, imgMeta) {
        const key = imgMeta.key;
        const url = global.scicat_url + "/api/v3/Datasets/" + key.replace("/", "%2F") + "/attachments";
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.props.token },
            body: JSON.stringify({
                "ownerGroup": this.props.proposalId,
                "thumbnail": img,
                "caption": imgMeta.name,
                "accessGroups": [this.props.beamline.toLowerCase()]
            })
        };

        fetch(url, requestOptions)
            .then((response) => {
                if (response.status === 200) {
                    this.refs["fileSelector_" + this.props.PID].value = null;
                    this.props.popUp("success", "Your image has been uploaded.");
                } else {
                    this.props.popUp("warning", "You don't have the correct access right to upload images");
                }
            })
            .catch(error => alert(error))
    }

    render() {
        return (
            <div>
                <input
                    type="file" multiple
                    ref={"fileSelector_" + this.props.PID}
                    accept=".jpeg, .jpg, .png, .svg"
                    onChange={this.updateImageSelection}>
                </input>
                <br></br>
                {this.state.upload && <button className="button" onClick={this.uploadImage}>Upload</button>}
            </div>
        );
    }
}
