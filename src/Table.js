import React, { Component } from 'react';
import DataTable from 'react-data-table-component';
import { Elogy } from "./Elogy"
import MultiSelectSort from './MultiSelectSort';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';


export class Table extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rows: [],
            noDataSelected: true,
            columns: [],
            fieldsChecked: false,
            sortQuery: this.props.sort,
        }

        this.handleChangeColumns = this.handleChangeColumns.bind(this);
        this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
        this.handleChangePageNumber = this.handleChangePageNumber.bind(this);
        this.handleSort = this.handleSort.bind(this);
        this.handleAddScanIDRange = this.handleAddScanIDRange.bind(this);
    }

    componentDidUpdate() {
        if (this.props.fields && !this.state.fieldsChecked) {
            this.getLocalStorage();
            this.setState({ fieldsChecked: true });
        }
    }

    getLocalStorage() {
        let columns = [];

        for (let k = 0; k < this.props.fields.length; k++) {
            let value = this.props.beamline + this.props.fields[k].value
            if (localStorage.getItem(value) && localStorage.getItem(value).includes("true")) {
                let id = localStorage.getItem(value).replace("true", "")
                columns.push(this.props.fields[k]);
                columns[columns.length - 1].id = parseInt(id);
            }
        }

        columns.sort((a, b) => (a.id > b.id) ? 1 : -1)
        this.setState({ columns: columns })

        if (columns.length == 0) {
            for (let k = 0; k < this.props.fields.length; k++) {
                if (this.props.fields[k].id < 100) {
                    columns.push(this.props.fields[k]);
                }
            }
            this.setState({ columns: columns })
        }
    }

    handleSelectedRowsChange = (e) => {
        this.setState({
           rows: e.selectedRows,
           noDataSelected: e.selectedRows.length == 0
        });
        this.props.onSelectedRowsChange(e.selectedRows);
    };

    handleAddScanIDRange() {
        let firstValue = this.refs.rangeID_first.value;
        let lastValue = this.refs.rangeID_last.value;

        let selected = this.props.data.filter( row => (row.scanID >= firstValue && row.scanID <= lastValue) || this.state.rows.includes(row));

        this.setState({
            rows: selected,
            noDataSelected: selected.length == 0
        });
        this.props.onSelectedRowsChange(selected);
    }

    handleChangeColumns(e) {
        for (let k = 0; k < this.props.fields.length; k++) {
            localStorage.setItem(this.props.beamline + this.props.fields[k].value, "false")
        }
        for (let k = 0; k < e.length; k++) {
            localStorage.setItem(this.props.beamline + e[k].value, k + "true")
        }
        this.setState({ columns: e });
    }

    handleChangeRowsPerPage(selectedRowsPerPage) {
        localStorage.setItem("rowsPerPage", selectedRowsPerPage);
        if(this.props.paginationServer){
            this.props.onChangeRowsPerPage(selectedRowsPerPage);
        }
    }

    handleChangePageNumber(selectedPage) {
        localStorage.setItem("pageNumber", selectedPage);
        if(this.props.paginationServer){
            this.props.onChangePageNumber(selectedPage);
        }
    }

    handleSort(column, sortDirection) {
        let sortQuery = "";
        switch (column.selector) {
            case "proposal":
                sortQuery = "proposalId";
                break;
            case "name":
                sortQuery = "datasetName";
                break;
            case "pi":
                sortQuery = "principalInvestigator";
                break;
            case "detectors":
                sortQuery = "scientificMetadata.detectors";
                break;
            case "start_time":
                sortQuery = "creationTime";
                break;

            default:
                sortQuery = "scientificMetadata.scanID";
                break;
        }
        sortQuery += ":" + sortDirection;

        this.props.onChangeSort(sortQuery);
    };

    render() {
        let columns = [];
        for (let k = 0; k < this.state.columns.length; k++) {
            columns[k] = {
                name: this.state.columns[k].label,
                selector: this.state.columns[k].value,
                sortable: this.state.columns[k].sortable,
                wrap: true, compact: true,
            }
            if (this.state.columns[k].widthFixed) {
                columns[k]["width"] = this.state.columns[k].width;
            } else {
                columns[k]["minWidth"] = this.state.columns[k].width;
            }
        }

        const conditionalRowStyles = [
            {
                when: row => row.scanlogGroup,
                style: row => ({
                    backgroundColor: row.scanlogGroup.color
                }),
            }
        ];

        return (
            <div>
                <Container style={{ paddingTop: "0.5rem" }} fluid>
                    <Row>
                        <Col xs={12}>
                            {this.state.fieldsChecked &&
                                <MultiSelectSort
                                    defaultValue={this.state.columns}
                                    options={this.props.fields}
                                    onChange={this.handleChangeColumns}
                                />
                            }
                        </Col>
                    </Row>
                    <Row style={{ paddingTop: "0.5rem" }}>
                        <Col xs={12}>
                            <DataTable
                                // Header columns and data
                                columns={columns}
                                data={this.props.data}
                                conditionalRowStyles={conditionalRowStyles}

                                //Layout
                                dense={true}
                                compact={true}
                                striped={true}
                                highlightOnHover={true}
                                pagination={true}
                                paginationRowsPerPageOptions={[10, 25, 50, 100]}

                                //pagination
                                paginationServer={this.props.paginationServer}
                                onChangePage={this.handleChangePageNumber}
                                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                paginationTotalRows={this.props.dataCount}
                                paginationDefaultPage={this.props.pageNumber}
                                paginationResetDefaultPage={this.props.resetTablePage}
                                paginationPerPage={this.props.rowsPerPage}

                                selectableRows={true}
                                selectableRowsHighlight={true}
                                onSelectedRowsChange={this.handleSelectedRowsChange}
                                selectableRowSelected={(row) => row.isSelected == true}

                                defaultSortField={"start_time"}
                                defaultSortAsc={false}
                                onSort={this.handleSort}
                                sortServer={this.props.sortServer}

                                theme={"solarized"}
                                noHeader={true}
                                noDataComponent={this.props.noData}

                                clearSelectedRows={this.props.clearSelectedRows}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col Col sm={3} md={8}>
                            <div className="fieldset">
                                <span className="legend">
                                    Add ScanID range
                                </span>
                                <Form.Group as={Row} className="mb-3">
                                    <Col sm="3">
                                        {<input
                                            className="form-control"
                                            ref="rangeID_first"
                                            placeholder="From"
                                        />}
                                    </Col>
                                    <Col sm="3">
                                        {<input
                                            className="form-control"
                                            ref="rangeID_last"
                                            placeholder="To"
                                        />}
                                    </Col>
                                    <Col xs={0.5}>
                                        <Button
                                            size="sm"
                                            variant="secondary"
                                            className='export-btn'
                                            onClick={this.handleAddScanIDRange}
                                            >Add ScanIDs
                                        </Button>
                                    </Col>
                                </Form.Group>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <Elogy
                                selectedRows={this.state.rows}
                                noDataSelected={this.state.noDataSelected}
                                activeColumns={columns}
                                username={this.props.username}
                                onGroupRows={this.props.onGroupRows}
                                popUp={this.props.popUp}
                            />
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
