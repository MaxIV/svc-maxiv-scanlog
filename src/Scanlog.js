import React, { Component } from 'react';
import './global.js'
import { Table } from "./Table";
import Comment from "./Comment";
import DQMStar from "./DQMStar";
import UploadImages from "./UploadImages";
import ShowImages from "./ShowImages";
import { DateRangePicker } from 'react-date-range';
import Toggle from 'react-toggle';
import moment from "moment";
require("react-toggle/style.css");
import { Container, Row, Col } from 'react-bootstrap';

export class Scanlog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scans: null,
            data: [],
            noData: "Connecting to SciCat...",
            selectionRange: {
                startDate: moment().subtract(1, 'days')._d,
                endDate: new Date(),
                key: 'selection',
            },
            selectionRangeUtc: {
                startDate: moment().subtract(1, 'days')._d,
                endDate: new Date(),
            },
            showDatepicker: false,
            notLoggedInMessage: "Not logged in, cannot access SciCat",
            fetchedData: false,
            proposalIds: null,
            filterByProposalId: localStorage.getItem(this.props.beamline + "filterByProposalId") || "all",
            filterByTime: false,
            fields: null,
            fieldsSkip: global.fieldsSkip,
            clearSelectedRows: false,
            pageNumber: parseInt(localStorage.getItem("pageNumber")) || 1,
            rowsPerPage: parseInt(localStorage.getItem("rowsPerPage")) || 10,
            sort: "creationTime:desc",
            dataCount: 0,
            dataCountFiltered: 0,
            resetTablePage: false,
        }
        this.fetchData = this.fetchData.bind(this);
        this.changeDays = this.changeDays.bind(this);
        this.datepickerToggle = this.datepickerToggle.bind(this);
        this.changeProposalID = this.changeProposalID.bind(this);
        this.onChangeComment = this.onChangeComment.bind(this);
        this.onGroupRows = this.onGroupRows.bind(this);
        this.handleSelectedRowsChange = this.handleSelectedRowsChange.bind(this);
    }

    componentDidMount() {
        let fieldsSkip = this.state.fieldsSkip
        for (let k = 0; k < global.fields.length; k++) {
            fieldsSkip.push(global.fields[k].label.toLowerCase())
        }

        this.setState({ fieldsSkip: fieldsSkip });

        this.fetchData();

        if (this.props.token !== "") {
            this.setProposalIDs();
        }
    }

    componentDidUpdate() {
        if (this.state.fetchedData && this.props.token === "") {
            this.setState({ fetchedData: false });
        }
        this.fetchData();
        if (this.state.resetTablePage) {
            this.setState({ resetTablePage: false });
        }
        if(this.state.proposalIds === null && this.props.token !== ""){
            this.setProposalIDs();
        }
    }

    fetchData() {
        if (this.props.loggedIn && !this.state.fetchedData && this.props.token !== "") {
            const base_url = global.scicat_url + "/api/v3/Datasets/fullquery";

            const storageProposalID = localStorage.getItem(this.props.beamline + "filterByProposalId");
            const proposalIDFilter = storageProposalID && storageProposalID !== "all" ? storageProposalID : "";

            const skip = (this.state.pageNumber - 1) * this.state.rowsPerPage;
            const limit = this.state.rowsPerPage;
            const sort = this.state.sort;

            let filter = {
                "fields": {
                    "mode": {},
                    "creationLocation": [this.props.beamline],
                    "proposalId": proposalIDFilter
                },
                "include": "attachments",
                "limits": {
                    "skip": skip,
                    "limit": limit,
                    "order": sort
                }
            };
            if (this.state.showDatepicker) {
                filter["fields"]["creationTime"] = {
                    "begin": new Date(this.state.selectionRangeUtc.startDate),
                    "end": new Date(this.state.selectionRangeUtc.endDate)
                };
                delete filter["limits"]["skip"];
                delete filter["limits"]["limit"];
            }

            if (proposalIDFilter !== "") {
                delete filter["limits"]["skip"];
                filter["limits"]["limit"] = 99999
                this.setState({resetTablePage: true})
            }
            if (proposalIDFilter == "") {
                delete filter["fields"]["proposalId"];
                if (skip === 0){
                    this.setState({resetTablePage: true})
                }
            }


            let fields = JSON.stringify(filter["fields"]);
            let limits = JSON.stringify(filter["limits"]);
            this.setState({ fetchedData: true });
            const url = base_url + "?fields=" + fields + "&limits=" + limits;
            const requestOptions = {
                headers: { 
                    'Content-Type': 'application/json', 
                    'Authorization': 'Bearer ' + this.props.token 
                }
            };
            fetch(url, requestOptions)
                .then(response => response.json())
                .then(data => {
                    this.setState({ scans: data, fetchedData: true });
                    if (this.state.filterByTime) {
                        this.setState({ dataCountFiltered: data.length });
                    }
                })
                .catch(error => alert(error))
                .then(() => { this.logData(this.state.filterByProposalId, this.state.filterByTime) })
        }
    }

    setProposalIDs() {

        const proposalIDOptions = [];

        const base_url = global.scicat_url + "/api/v3/datasets/";
        const filter = JSON.stringify({ "mode": {}, "creationLocation": [this.props.beamline] });
        const facets = JSON.stringify(["ownerGroup"]);
        const url = base_url + 'fullfacet?fields=' + filter + '&facets=' + facets;
        const requestOptions = {
            headers: { 
                'Content-Type': 'application/json', 
                'Authorization': 'Bearer ' + this.props.token 
            }
        };
        let totalCount = 0;
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                data = data[0];
                for (let k = 0; k < data.ownerGroup.length; k++) {
                    const item = data.ownerGroup[k];
                    item._id = item._id.replace("-group", "");
                    totalCount += item.count;
                    proposalIDOptions.push(<option value={item._id} data-count={item.count} key={item._id}>{item._id} ({item.count})</option>);
                }

                const proposalID = localStorage.getItem(this.props.beamline + "filterByProposalId");
                this.setState({
                    proposalIds: [
                        <option value="default" disabled key="default">Select proposal</option>,
                        <option value="all" data-count={totalCount} key="all">All proposals ({totalCount})</option>
                    ].concat(proposalIDOptions),
                    dataCount: proposalID && proposalID != "all" && data.ownerGroup.length != 0 ? data.ownerGroup.filter(obj => { return obj._id === proposalID.toString() })[0].count : totalCount
                });
            })
            .catch(error => alert(error));
    }

    logData(filterByProposalId = "all", filterByTime = true) {
        //Modify the incoming data such that it's functional with the table
        if (this.state.scans === null) {
            return null;
        }

        const data = [];
        let fields = [];
        let fieldsList = []

        let scanlogGroupedData = localStorage.getItem("scanlogGroupedData");
        scanlogGroupedData = JSON.parse(scanlogGroupedData) || [];
        for (let k = 0; k < this.state.scans.length; k++) {
            // Only get the data of the most recent X days
            const startDate = this.state.selectionRange.startDate.toISOString();
            const endDate = this.state.selectionRange.endDate.toISOString();

            let scan = <i>undefined</i>;
            let detectors = <i>undefined</i>;
            let start_time = "";
            let scanID = <i>undefined</i>;
            let scicat_url = "";

            if (this.state.scans[k].scientificMetadata.scanID) {
                scanID = this.state.scans[k].scientificMetadata.scanID.value ? parseInt(this.state.scans[k].scientificMetadata.scanID.value) : parseInt(this.state.scans[k].scientificMetadata.scanID);
            }

            if (this.state.scans[k].scientificMetadata.start_time) {
                if (typeof this.state.scans[k].scientificMetadata.start_time === 'object' && this.state.scans[k].scientificMetadata.start_time !== null) {
                    start_time = this.state.scans[k].scientificMetadata.start_time.value;
                }
                else {
                    start_time = this.state.scans[k].scientificMetadata.start_time;
                }
            }
            else {
                start_time = this.state.scans[k].createdAt;
            }

            if (filterByTime && (startDate > start_time || endDate < start_time)) {
                continue;
            }

            if (filterByProposalId != "all" && filterByProposalId != "default") {
                if (typeof this.state.scans[k].proposalId == "undefined" || this.state.scans[k].proposalId.toString() != filterByProposalId) {
                    continue;
                }
            }

            //TODOS: we're updating the start_time above also? the line below will overwrite it.
            start_time = new Date(new Date(start_time).getTime() - (new Date(start_time).getTimezoneOffset() * 60000)).toISOString().replace("T", " ").replace("Z", " ").slice(0, 23);

            if (this.state.scans[k].scientificMetadata.title) {
                if (typeof this.state.scans[k].scientificMetadata.title === 'object' && this.state.scans[k].scientificMetadata.title !== null) {
                    scan = this.state.scans[k].scientificMetadata.title.value;
                }
                else {
                    scan = this.state.scans[k].scientificMetadata.title;
                }
            }

            if (this.state.scans[k].scientificMetadata.detectors) {
                if (this.state.scans[k].scientificMetadata.detectors.value) {
                    if (typeof this.state.scans[k].scientificMetadata.detectors.value === "string") {
                        detectors = Array(this.state.scans[k].scientificMetadata.detectors.value);
                    } else {
                        detectors = this.state.scans[k].scientificMetadata.detectors.value.join("; ");
                    }
                } else {
                    if (typeof this.state.scans[k].scientificMetadata.detectors === "string") {
                        detectors = Array(this.state.scans[k].scientificMetadata.detectors)
                    } else if (this.state.scans[k].scientificMetadata.detectors.length > 0) {
                        detectors = this.state.scans[k].scientificMetadata.detectors.join("; ");
                    }
                }
            }

            scicat_url = global.scicat_url + "/datasets/" + this.state.scans[k].pid.replace("/", "%2F");

            if (!this.state.scans[k].comment) {
                this.state.scans[k].comment = ""
            }

            this.state.scans[k].origComment = this.state.scans[k].comment
            this.state.scans[k].comments = {
                comment: this.state.scans[k].comment,
                origComment: this.state.scans[k].origComment
            }


            let dataset = {
                id: k,
                scanID: scanID,
                start_time: start_time,
                pi: this.state.scans[k].principalInvestigator,
                proposal: this.state.scans[k].proposalId,
                name: this.state.scans[k].datasetName.slice(0, 1).toUpperCase() + this.state.scans[k].datasetName.slice(1, this.state.scans[k].datasetName.length),
                scan: scan,
                detectors: detectors,
                path: this.state.scans[k].sourceFolder,
                scicat: <a className="scicatlink" target='blank' name={scicat_url} href={scicat_url}> Link </a>,
                comment: <Comment
                    id ={k}
                    PID={this.state.scans[k].pid}
                    comments={this.state.scans[k].comments}
                    token={this.props.token}
                    changeComment={this.onChangeComment}
                    popUp={this.props.popUp}
                />,
                show_img: this.state.scans[k].attachments.length == 0 ? "No images" : <ShowImages
                    PID={this.state.scans[k].pid}
                    token={this.props.token}
                    name={this.state.scans[k].datasetName}
                    attachments={this.state.scans[k].attachments} />,
                upload_img: <UploadImages
                    PID={this.state.scans[k].pid}
                    proposalId={this.state.scans[k].proposalId + "-group"}
                    token={this.props.token}
                    beamline={this.props.beamline}
                    popUp={this.props.popUp}/>,
                dqm: <DQMStar
                    PID={this.state.scans[k].pid}
                    dqm={this.state.scans[k].dataQualityMetrics}
                    name={this.state.scans[k].datasetName}
                    token={this.props.token}/>,
            }
            //set scanlogGroup from localStorage
            scanlogGroupedData.map((dat) => {
                if (dat.pid == this.state.scans[k].pid) {
                    dataset.scanlogGroup = dat.scanlogGroup;
                }
            });

            for (let metaname in this.state.scans[k].scientificMetadata) {
                if (!this.state.fieldsSkip.includes(metaname.toLowerCase())) {
                    if (!fieldsList.includes(metaname)) {
                        fields.push({
                            id: fields.length + 100,
                            label: metaname,
                            value: metaname.replace(" ", "_").toLowerCase(),
                        })
                        fieldsList.push(metaname)
                    }
                    let metavariable = this.state.scans[k].scientificMetadata[metaname]

                    try {
                        if (typeof (metavariable) !== "object") {
                            dataset[metaname.replace(" ", "_").toLowerCase()] = metavariable
                        } else if (typeof (metavariable) === "object") {
                            if (metavariable.value) {
                                dataset[metaname.replace(" ", "_").toLowerCase()] = metavariable.value + " " + metavariable.unit
                            } else {
                                let metastring = []
                                for (let key in metavariable) {
                                    if ("value" in metavariable[key]) {
                                        metastring.push({"value": metavariable[key].value, "display": key + " " + metavariable[key].value + " " + metavariable[key].unit + ";", "key": metaname + " - " + key + " (" + metavariable[key].unit + ")"})
                                    } else if ("begin" in metavariable[key]) {
                                        metastring.push({"value": "[" + metavariable[key]["begin"].value + " ; " + metavariable[key]["end"].value + "]", "display": key + " " + metavariable[key]["begin"].value + "; " + metavariable[key]["end"].value + " " + metavariable[key]["begin"].unit + ";", "key": metaname + " - " + " (" + metavariable[key]["begin"].unit + ")"})
                                    }
                                }
                                dataset[metaname.replace(" ", "_").toLowerCase()] = metastring.map(str => <p value={str.value} key={str.key.replace(" ()", "")}>{str.display}</p>);
                            }
                        }
                    }
                    catch {
                        console.log("Cannot write metadata of " + metaname)
                    }
                }
            };
            //Set data per scan
            data.push(dataset);
        }
        if (data.length === 0) {
            this.setState({ noData: "No recent scans found" });
        }
        this.setState({
            data: data,
            fields: [...global.fields, ...fields],
        });
        if(filterByProposalId !== "all" || filterByTime){
            this.setState({clearSelectedRows: true }, () => {
                setTimeout(() => {
                    this.setState({ clearSelectedRows: false });
                }, 2000);
            });
        }
    }

    changeDays(e) {
        const selectedDate = e.selection;
        const dateRangeUTC = {
            startDate: moment(selectedDate.startDate).utc().format(),
            endDate: moment(selectedDate.endDate).utc().format()
        }

        this.setState({
            selectionRange: selectedDate,
            selectionRangeUtc: dateRangeUTC,
            fetchedData: false
        }, function () {
            this.fetchData();
        }
        )
    }

    datepickerToggle(e) {
        this.setState({
            fetchedData: false,
            showDatepicker: e.target.checked,
            pageNumber: 1,
            resetTablePage: true
        });
    }

    onChangeComment(id, comment, new_comment) {
        let scans = this.state.scans;
        scans[id].comments.comment = new_comment;
        scans[id].comments.origComment = comment;
        this.setState({
            scans: scans,
        });
    }


    changeProposalID(e) {
        localStorage.setItem(this.props.beamline + "filterByProposalId", e.target.value);
        localStorage.setItem("pageNumber", "1");

        this.setState({
            filterbyTime: false,
            fetchedData: false,
            filterByProposalId: e.target.value,
            dataCount: e.target.selectedOptions[0].getAttribute("data-count"),
            pageNumber: 1,
        });
    }

    onGroupRows(selectedScanlogGroup, selectedRows) {
        const data = this.state.data;
        const scanlogGroupedData = JSON.parse(localStorage.getItem("scanlogGroupedData")) || [];

        data.map((row) => {
            selectedRows.map((selectedRow) => {
                //TODOS: refine this
                if (selectedRow.id == row.id) {
                    const href = row.scicat.props.href;
                    const pid = href && href.substring(href.indexOf("datasets/") + 9);
                    scanlogGroupedData.push({ pid: decodeURIComponent(pid), scanlogGroup: selectedScanlogGroup });
                    row.scanlogGroup = selectedScanlogGroup;
                }
            });
        });
        localStorage.setItem("scanlogGroupedData", JSON.stringify(scanlogGroupedData));

        this.setState({ data: data, clearSelectedRows: true }, () => {
            setTimeout(() => {
                this.setState({ clearSelectedRows: false });
            }, 2000);
        });
    }

    handleSelectedRowsChange(selected) {
        this.setState(prevState => ({
            data: prevState.data.map(row => {
                row.isSelected = !!selected.find(e => e.id === row.id)
                return row
              })
        }))
    }

    render() {
        let formatStartDate = moment(this.state.selectionRange.startDate).format("YYYY-MM-DD hh:mm:ss");
        let formatEndDate = moment(this.state.selectionRange.endDate).format("YYYY-MM-DD hh:mm:ss");

        return (
            <div className="Scanlog">
                {this.props.loggedIn ?
                    <div className="content">
                        <Container fluid>
                            <Row>
                                <Col sm={4}>
                                    <div className="toggle">
                                        <Toggle
                                            checked={this.state.showDatepicker}
                                            name='showDatepicker'
                                            value='yes'
                                            onChange={this.datepickerToggle} />
                                        {this.state.showDatepicker ?
                                            <p>&nbsp;Showing scans from {formatStartDate} till {formatEndDate}</p>
                                            :
                                            <p>&nbsp;Showing all scans from selected Proposal ID</p>
                                        }
                                    </div>
                                    {this.state.showDatepicker &&
                                        <DateRangePicker
                                            ranges={[this.state.selectionRange]}
                                            onChange={this.changeDays}
                                        />
                                    }
                                </Col>
                                <Col sm={{ span: 2, offset: 6 }}>
                                    <div className="toggle_right">
                                        <select className="form-control" onChange={this.changeProposalID} value={this.state.filterByProposalId}>
                                            {this.state.proposalIds}
                                        </select>
                                    </div>
                                </Col>
                            </Row>
                        </Container>


                        <Table
                            data={this.state.data}
                            noData={this.state.noData}
                            username={this.props.username}
                            beamline={this.props.beamline}
                            fields={this.state.fields}
                            onChangeSort={(sort) => {
                                this.setState({
                                    fetchedData: false,
                                    sort: sort
                                })
                            }}
                            onChangeRowsPerPage={(rowsPerPage) => {
                                this.setState({
                                    fetchedData: false,
                                    rowsPerPage: rowsPerPage
                                })
                            }}
                            onChangePageNumber={(pageNumber) => {
                                this.setState({
                                    fetchedData: false,
                                    pageNumber: pageNumber
                                })
                            }}
                            onSelectedRowsChange={(selectedIDs) => {
                                this.handleSelectedRowsChange(selectedIDs);
                            }}
                            dataCount={parseInt(this.state.showDatepicker ? this.state.dataCountFiltered : this.state.dataCount)}
                            paginationServer={this.state.showDatepicker || this.state.filterByProposalId == "all"}
                            sortServer={this.state.showDatepicker ? false : true}
                            resetTablePage={this.state.resetTablePage}

                            onGroupRows={this.onGroupRows}
                            clearSelectedRows={this.state.clearSelectedRows}

                            rowsPerPage={this.state.rowsPerPage}
                            pageNumber={this.state.pageNumber}
                            sort={this.state.sort}
                            popUp={this.props.popUp}
                        />
                    </div>
                    :
                    <p className="content2">{this.state.notLoggedInMessage}</p>
                }
            </div>
        );
    }

}
