import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';
import './global.js';
import ReactStars from 'react-stars';

export default class DQMStar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showSuccess: false,
            showFail: false,
            showDelete: this.props.dqm ? true : false,
            score: this.props.dqm,
            setScore: ""
        }

        this.DQMRating = this.DQMRating.bind(this)
        this.deleteDQM = this.deleteDQM.bind(this)
    }

    DQMRating = (score) => {
        this.setState({ score: score, setScore: score });
        const url = global.scicat_url + "/api/v3/Datasets/" + this.props.PID.replace("/", "%2F");

        const requestOptions = {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.props.token },
            body: JSON.stringify({ "dataQualityMetrics": score })
        };
        fetch(url, requestOptions)
            .then((response) => {
                response.status === 200 ? this.popUp() : this.popUpFail();
            })
            .catch(error => alert(error))
    }

    popUp() {
        if (this.state.score !== null) {
            this.setState({ showDelete: true });
        } else {
            this.setState({ setScore: "no" });
        }
        this.setState(({ showSuccess: true }));
        setTimeout(() => { this.setState({ showSuccess: false }) }, 2000);
    }

    popUpFail() {
        this.setState(({ showFail: true, score: this.props.dqm }));
        setTimeout(() => { this.setState({ showFail: false }) }, 2000);
    }

    deleteDQM() {
        this.DQMRating(null);
        this.setState({ showDelete: false });
    }

    render() {
        return (
            <div>
                <div id="DQMStars">
                    <ReactStars
                        count={3}
                        onChange={this.DQMRating}
                        size={24}
                        color2={'#ffd700'}
                        value={this.state.score} />
                </div>
                {this.state.showDelete && <p><button className="deleteDQM" onClick={this.deleteDQM}>❌</button></p>}
                {this.state.showSuccess &&
                    <div id="popup">
                        <Alert key="dqm" variant="success">
                            You rated {this.props.name} with {this.state.setScore} stars
                        </Alert>
                    </div>
                }
                {this.state.showFail &&
                    <div id="popup">
                        <Alert key="dqm" variant="warning">
                            You don't have the correct access right to rate scans
                        </Alert>
                    </div>
                }
            </div>
        );
    }
}
