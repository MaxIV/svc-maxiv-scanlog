import React, { Component } from 'react';
import './global.js'
import { Button } from 'react-bootstrap';
import { CSVLink } from "react-csv";
import { Container, Row, Col } from 'react-bootstrap';

export class Export extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            user: null,
            oldData: "",
            exportData: null,
            amountofDatasets: 0,
            selectedLogbookID: 0,
            dataSelected: true,
            CSVexportData: [],
            CSVheaders: [],
            data: {}
        }
        this.polishData = this.polishData.bind(this);
        this.exporttoCSV = this.exporttoCSV.bind(this);
        this.csvLinkEl = React.createRef();
    }

    getExportedData() {
        let exportData = [];
        let selectedRowsSorted = this.props.selectedRows.sort((a,b) => a.id > b.id ? 1 : -1)
        for (let k = 0; k < selectedRowsSorted.length; k++) {
            let obj = {};
            for (let j = 0; j < this.props.activeColumns.length; j++) {
                let key = this.props.activeColumns[j].selector;
                let label = this.props.activeColumns[j].name;
                let value = selectedRowsSorted[k][key];
                if (key == "upload_img" || key == "id") {
                    continue;
                } else if (key == "comment") {
                    value = selectedRowsSorted[k][key].props.comments.origComment;
                }
                if (typeof value === 'object') {
                    if (key == "scicat")
                        obj[label] = value.props["name"];
                    else if (Array.isArray(value)){
                        for (let i = 0; i < value.length; i++ ){
                            obj[value[i]["key"]] = value[i].props.value
                        }
                    }
                    else
                        obj[label] = value.props[key];
                } else {
                    obj[label] = value;
                }
            }
            exportData[k] = obj;
        }
        return exportData

    }


    polishData() {
        //Polish data
        let exportData = this.getExportedData()

        let content = "";

        if (this.props.newEntry === false) {
            if (!("content" in this.props.selectedEntry)) {
                alert("Can't send data to Elogy, please select an entry to append");
                return null;
            }
            this.retrieveEntry();
        }
        for (let k = 0; k < exportData.length; k++) {
            if (k == 0) {
                //Create header
                content += "<br />\n<table>\n<tbody>\n><tr>"
                for (const [key, value] of Object.entries(exportData[k])) {
                    content += "\n<td>"
                    content += key
                    content += "</td>"
                }
                content += "\n</tr>"
            }
            content += "\n<tr>"
            for (const [key, value] of Object.entries(exportData[k])) {
                content += "\n<td>"
                content += value
                content += "</td>"
            }
            content += "\n</tr>"

            if ((k + 1) == exportData.length) {
                //Create footer
                content += "\n</tbody>\n</table>"
            }

        }
        this.setState({ exportData: content, amountofDatasets: exportData.length });
        this.findUser();

    }

    findUser() {
        const url = global.elogy_url + "/users?search=" + this.props.username

        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({ user: data.users }))
            .catch(error => alert(error))
            .then(() => { this.confirmData() })
    }

    retrieveEntry() {
        const url = global.elogy_url + "/logbooks/" + this.state.selectedLogbookID + "/entries/" + this.props.selectedEntry.id;

        fetch(url)
            .then(response => response.json())
            .then(data => this.setState({ oldData: data.entry }))
            .catch(error => alert(error))
    }


    confirmData() {
        let user = {};

        for (let k = 0; k < this.state.user.length; k++) {
            if (this.state.user[k].login == this.props.username) {
                user = this.state.user[k]
            }
        }
        if (Object.entries(user).length === 0) {
            alert("Can't send data to Elogy, this user is not allowed to create entries");
            return null;
        }

        if (this.props.selectedLogbook === "") {
            alert("Can't send data to Elogy, please select a logbook");
            return null;
        }

        let elogy_title = "";

        if (this.props.newEntry === true) {
            elogy_title = this.props.refs["elogy_title"].value;

            if (elogy_title == "") {
                elogy_title = "Exported from Scanlog";
            }
            const data = {
                "title": elogy_title,
                "authors": user,
                "content": this.state.exportData,
            }
            this.setState({ data: data });
        } else {
            const data = {
                "authors": user,
                "content": this.state.oldData.content + this.state.exportData,
                "revision_n": this.state.oldData.revision_n,
            };
            this.setState({ data: data });
            elogy_title = this.props.selectedEntry.title;
        }

        if (window.confirm("You are now exporting the data of " + this.state.amountofDatasets + " scans to Elogy. \n Title: " + elogy_title + "\n Author: " + user.name + "\n Logbook: " + this.props.selectedLogbook)) {
            this.sendtoElogy();
        }

    }

    sendtoElogy() {
        let url = global.elogy_proxyurl + "/logbooks/" + this.props.selectedLogbookID + "/entries/";
        let requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.data)
        };

        if (this.props.newEntry === false) {
            url += this.props.selectedEntry.id + "/";

            requestOptions = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(this.state.data)
            };

        }

        fetch(url, requestOptions)
            .then((response) => { if (response.status === 200) { this.props.popUp("success", "Your data has been exported to Elogy"); } })
            .catch(error => alert(error))

    }

    exporttoCSV() {
        let exportData = this.getExportedData();

        let headers = [];
        for (let key in exportData[0]) {
            headers.push({ key: key, label: key })
        }

        this.setState({ CSVexportData: exportData, CSVheaders: headers }, () => {
            setTimeout(() => {
                this.csvLinkEl.current.link.click();
            });
        });
    }

    render() {
        return (
            <div>
                <Container fluid style={{ marginTop: "1rem" }}>
                    <Row>
                        <Col sm={{ span: 5, offset: 3 }}>
                            <Row>
                                <Col xs={12}>
                                    <Button size="sm" variant="success" disabled={!(!this.props.noDataSelected && this.props.logbookSelected)} onClick={this.polishData} className='export-btn'>Export to Elogy</Button>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm={12}>
                            <hr />
                        </Col>
                        <Col sm={{ span: 5, offset: 3 }}>
                            <Row>
                                <Col xs={12}>
                                    <Button size="sm" variant="success" disabled={this.props.noDataSelected} onClick={this.exporttoCSV} className='export-btn'>Export to CSV</Button>
                                </Col>
                            </Row>
                        </Col>

                    </Row>
                </Container>
                <CSVLink
                    headers={this.state.CSVheaders}
                    filename="Scanlog_Data.csv"
                    data={this.state.CSVexportData}
                    ref={this.csvLinkEl}
                />
            </div>
        );
    }
}
