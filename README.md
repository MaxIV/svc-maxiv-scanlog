# svc-maxiv-scanlog

Metadata scanlog viewer for SciCat. Displays the data from each scan, by default sorted by most recent scan, with some of their main metadata parameters displayed.

## How to run locally

Add a .env file in the repo and define:
```
REACT_APP_SCICATURL=https://scicat-test.maxiv.lu.se
```

In terminal:

```
cd svc-maxiv-scanlog
npm install
npm start
```

In browser:
`http://localhost:3000`


## Backend

The Scanlog does not have its own backend, it completely relies on the SciCat database using the LoopBack API. The Login is done via SciCat as well with the MAXIV Authorization.

## How to use

In Scanlog, there are two ways to filter which scans you want to see in the table: on the top left it can be seen that a time-frame is selected of which scans should be shown. This can be adjusted by clicking the toggle to the left and selecting a different time in the window that pops up. On the top right, a proposal ID can be selected, which will show only scans from that proposal ID.

In the row underneath the table columns can be selected. By clicking on the cross next to one of the headers in that row, that metadata column will be hidden in the table. The same way columns can be added by clicking in that row and select an item from the drop-down list. The order can also be changed by dragging the headers around. These settings are saved in the browser on the computer that is used.

In the actual table, the metadata is shown row by row per scan. A link to its respective SciCat page can be found by clicking the green "Link" button. Furthermore, it is possible to write a comment about the specific scan, a green "Save" button will appear as soon as you start typing in the comment section. It is also possible to upload an image by using the "Browse..." button in the upload image column. These images can be seen on the page in SciCat, or by clicking the green "Show" button in the images column. Last, it is possible to rate scans as Data Quality Metrics. A scan has not been rated if there are no stars, and can have a minimum of half a star and a maximum of three stars rating, depending on how well you think the scan went.

It is possible to export data from the table by clicking the selectable button on the left of each row. It is possible to export data to Elogy, which will either create a new entry or append to an already existing entry in a chosen logbook. When creating a new entry, the title of this entry can be specified or use the default title, "Exported from Scanlog". Only the columns that are currently visible in the table will be exported to Elogy.

It is also possible to export to a CSV file, which will be automatically downloaded.
